/*
 *************************************************************************
 *  Copyright (c) Metabiota Incorporated - All Rights Reserved
 *------------------------------------------------------------------------
 *  This material is proprietary to Metabiota Incorporated. The
 *  intellectual and technical concepts contained herein are proprietary
 *  to Metabiota Incorporated. Reproduction or distribution of this
 *  material, in whole or in part, is strictly forbidden unless prior
 *  written permission is obtained from Metabiota Incorporated.
 *************************************************************************
 */

package com.metabiota.eventsCaching;

public class EventDTO {

    /*
    ROW_TYPE','GROUPING_ID','CC',
    'PATHOGEN','EVENT_ID','EVENT',
    'SRC','COUNTRY','REGION',
    'DISEASE','DATE_START','DATE_END',
    'GLOBAL_DATE_START', 'GLOBAL_DATE_END','DURATION',
    'COUNTRY_ORIG','CFR','ENDEMIC',
    'CUML_CASES','CUML_DEATHS','R0_ML_MIN',
    'R0_ML_MAX','R0_ML','R0_EG_MIN',
    'R0_EG_MAX','R0_EG','DT_ML_MIN',
    'DT_ML_MAX','DT_ML','DT_EG_MIN',
    'DT_EG_MAX','DT_EG'
     */

    private Integer countryCodeId;
    private Integer pathogenId;

    private Integer eventId;

    private String eventName;

    private String eventType;

    private Long startDate;

    private Long endDate;

    private Long globalStartDate;

    private Long globalEndDate;

    private Integer cumlCases;

    private Integer cumlDeaths;

    private Double cfr;

    private String country;

    private String countryCode;

    private String countryOrig;

    private String disease;

    private String pathogen;

    private Double r0EG;

    private Double r0ML;

    private String src;

    private Integer cumlEstdCasesLow;

    private Integer cumlEstdCasesHigh;

    private Integer cumlEstdDeathsLow;

    private Integer cumlEstdDeathsHigh;

    private Integer cumlSuspectedCases;

    private Integer cumlProbableCases;

    private Integer cumlConfirmedCases;

    private Integer cumlSuspectedDeaths;

    private Integer cumlProbableDeaths;

    private Integer cumlConfirmedDeaths;

    public Integer getEventId() {
        return eventId;
    }

    public EventDTO setEventId(Integer eventId) {
        this.eventId = eventId;
        return this;
    }

    public String getEventName() {
        return eventName;
    }

    public EventDTO setEventName(String eventName) {
        this.eventName = eventName;
        return this;
    }

    public String getEventType() {
        return eventType;
    }

    public EventDTO setEventType(String eventType) {
        this.eventType = eventType;
        return this;
    }

    public Long getStartDate() {
        return startDate;
    }

    public EventDTO setStartDate(Long startDate) {
        this.startDate = startDate;
        return this;
    }

    public Long getEndDate() {
        return endDate;
    }

    public EventDTO setEndDate(Long endDate) {
        this.endDate = endDate;
        return this;
    }

    public Long getGlobalStartDate() {
        return globalStartDate;
    }

    public EventDTO setGlobalStartDate(Long globalStartDate) {
        this.globalStartDate = globalStartDate;
        return this;
    }

    public Long getGlobalEndDate() {
        return globalEndDate;
    }

    public EventDTO setGlobalEndDate(Long globalEndDate) {
        this.globalEndDate = globalEndDate;
        return this;
    }

    public Integer getCumlCases() {
        return cumlCases;
    }

    public EventDTO setCumlCases(Integer cumlCases) {
        this.cumlCases = cumlCases;
        return this;
    }

    public Integer getCumlDeaths() {
        return cumlDeaths;
    }

    public EventDTO setCumlDeaths(Integer cumlDeaths) {
        this.cumlDeaths = cumlDeaths;
        return this;
    }

    public Double getCfr() {
        return cfr;
    }

    public EventDTO setCfr(Double cfr) {
        this.cfr = cfr;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public EventDTO setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public EventDTO setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public String getCountryOrig() {
        return countryOrig;
    }

    public EventDTO setCountryOrig(String countryOrig) {
        this.countryOrig = countryOrig;
        return this;
    }

    public String getDisease() {
        return disease;
    }

    public EventDTO setDisease(String disease) {
        this.disease = disease;
        return this;
    }

    public String getPathogen() {
        return pathogen;
    }

    public EventDTO setPathogen(String pathogen) {
        this.pathogen = pathogen;
        return this;
    }

    public Double getR0EG() {
        return r0EG;
    }

    public EventDTO setR0EG(Double r0EG) {
        this.r0EG = r0EG;
        return this;
    }

    public Double getR0ML() {
        return r0ML;
    }

    public EventDTO setR0ML(Double r0ML) {
        this.r0ML = r0ML;
        return this;
    }

    public String getSrc() {
        return src;
    }

    public EventDTO setSrc(String src) {
        this.src = src;
        return this;
    }

    public Integer getCumlEstdCasesLow() {
        return cumlEstdCasesLow;
    }

    public EventDTO setCumlEstdCasesLow(Integer cumlEstdCasesLow) {
        this.cumlEstdCasesLow = cumlEstdCasesLow;
        return this;
    }

    public Integer getCumlEstdCasesHigh() {
        return cumlEstdCasesHigh;
    }

    public EventDTO setCumlEstdCasesHigh(Integer cumlEstdCasesHigh) {
        this.cumlEstdCasesHigh = cumlEstdCasesHigh;
        return this;
    }

    public Integer getCumlEstdDeathsLow() {
        return cumlEstdDeathsLow;
    }

    public EventDTO setCumlEstdDeathsLow(Integer cumlEstdDeathsLow) {
        this.cumlEstdDeathsLow = cumlEstdDeathsLow;
        return this;
    }

    public Integer getCumlEstdDeathsHigh() {
        return cumlEstdDeathsHigh;
    }

    public EventDTO setCumlEstdDeathsHigh(Integer cumlEstdDeathsHigh) {
        this.cumlEstdDeathsHigh = cumlEstdDeathsHigh;
        return this;
    }

    public Integer getCumlSuspectedCases() {
        return cumlSuspectedCases;
    }

    public EventDTO setCumlSuspectedCases(Integer cumlSuspectedCases) {
        this.cumlSuspectedCases = cumlSuspectedCases;
        return this;
    }

    public Integer getCumlProbableCases() {
        return cumlProbableCases;
    }

    public EventDTO setCumlProbableCases(Integer cumlProbableCases) {
        this.cumlProbableCases = cumlProbableCases;
        return this;
    }

    public Integer getCumlConfirmedCases() {
        return cumlConfirmedCases;
    }

    public EventDTO setCumlConfirmedCases(Integer cumlConfirmedCases) {
        this.cumlConfirmedCases = cumlConfirmedCases;
        return this;
    }

    public Integer getCumlSuspectedDeaths() {
        return cumlSuspectedDeaths;
    }

    public EventDTO setCumlSuspectedDeaths(Integer cumlSuspectedDeaths) {
        this.cumlSuspectedDeaths = cumlSuspectedDeaths;
        return this;
    }

    public Integer getCumlProbableDeaths() {
        return cumlProbableDeaths;
    }

    public EventDTO setCumlProbableDeaths(Integer cumlProbableDeaths) {
        this.cumlProbableDeaths = cumlProbableDeaths;
        return this;
    }

    public Integer getCumlConfirmedDeaths() {
        return cumlConfirmedDeaths;
    }

    public EventDTO setCumlConfirmedDeaths(Integer cumlConfirmedDeaths) {
        this.cumlConfirmedDeaths = cumlConfirmedDeaths;
        return this;
    }

    public Integer getCountryCodeId() {
        return countryCodeId;
    }

    public EventDTO setCountryCodeId(Integer countryCodeId) {
        this.countryCodeId = countryCodeId;
        return this;
    }

    public Integer getPathogenId() {
        return pathogenId;
    }

    public EventDTO setPathogenId(Integer pathogenId) {
        this.pathogenId = pathogenId;
        return this;
    }
}
