/*
 *************************************************************************
 *  Copyright (c) Metabiota Incorporated - All Rights Reserved
 *------------------------------------------------------------------------
 *  This material is proprietary to Metabiota Incorporated. The
 *  intellectual and technical concepts contained herein are proprietary
 *  to Metabiota Incorporated. Reproduction or distribution of this
 *  material, in whole or in part, is strictly forbidden unless prior
 *  written permission is obtained from Metabiota Incorporated.
 *************************************************************************
 */

package com.metabiota.eventsCaching.util;

import javax.annotation.Nonnull;
import java.util.Properties;

public class TypedProperties {

    private Properties props;

    public static TypedProperties from(Properties props) {
        return new TypedProperties(props);
    }

    public TypedProperties(Properties props) {
        this.props = props;
    }

    public String getString(@Nonnull String name, String defaultValue) {
        final String s = (String) props.get(name);
        return (isEmpty(s)) ? defaultValue : s;
    }

    public Boolean getBoolean(@Nonnull String name, Boolean defaultValue) {
        final String s = (String) props.get(name);
        return (isEmpty(s)) ? defaultValue : Boolean.parseBoolean(s);
    }

    public Integer getInteger(@Nonnull String name, Integer defaultValue)  {
        final String s = (String) props.get(name);
        return (isEmpty(s)) ? defaultValue : Integer.parseInt(s);
    }

    private boolean isEmpty(String val) {
        return (val == null || val.trim().isEmpty());
    }

    @Override
    public String toString() {
        return "TypedProperties{" +
                "props=" + props +
                '}';
    }
}
