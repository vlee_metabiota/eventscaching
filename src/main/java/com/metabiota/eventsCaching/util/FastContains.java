/*
 *************************************************************************
 *  Copyright (c) Metabiota Incorporated - All Rights Reserved
 *------------------------------------------------------------------------
 *  This material is proprietary to Metabiota Incorporated. The
 *  intellectual and technical concepts contained herein are proprietary
 *  to Metabiota Incorporated. Reproduction or distribution of this
 *  material, in whole or in part, is strictly forbidden unless prior
 *  written permission is obtained from Metabiota Incorporated.
 *************************************************************************
 */

package com.metabiota.eventsCaching.util;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Class to do quick 'contains' comparisons for sets of limited size.
 */
public class FastContains {

    private Map<String, Integer> membershipIds;

    public FastContains(Set<String> allMembers) {
        this.membershipIds = new HashMap<>();

        int i = 0;
        for (String member : allMembers) {
            this.membershipIds.put(member, i++);
        }
    }

    public Integer getMembershipId(String member) {
        return membershipIds.get(member);
    }

    public BitSet createMembership(Set<String> members) {
        BitSet membership = new BitSet(members.size());
        for (String member : members) {
            Integer id = getMembershipId(member);
            assert (id != null);
            membership.set(id);
        }
        return membership;
    }
}
