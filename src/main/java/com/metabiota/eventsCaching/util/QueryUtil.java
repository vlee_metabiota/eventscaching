/*
 *************************************************************************
 *  Copyright (c) Metabiota Incorporated - All Rights Reserved
 *------------------------------------------------------------------------
 *  This material is proprietary to Metabiota Incorporated. The
 *  intellectual and technical concepts contained herein are proprietary
 *  to Metabiota Incorporated. Reproduction or distribution of this
 *  material, in whole or in part, is strictly forbidden unless prior
 *  written permission is obtained from Metabiota Incorporated.
 *************************************************************************
 */

package com.metabiota.eventsCaching.util;

import com.metabiota.eventsCaching.EventDTO;

import java.util.Comparator;

public class QueryUtil {

    public static Comparator<EventDTO> getOrderBy(String orderByCriteria) {
        switch (orderByCriteria == null ? "" : orderByCriteria) {
            case "cumlCases":
                return Comparator.comparingInt(EventDTO::getCumlCases).reversed();

            case "cumlDeaths":
                return Comparator.comparingInt(EventDTO::getCumlDeaths).reversed();

            case "cfr":
                return Comparator.comparingDouble(EventDTO::getCfr).reversed();

            case "country":
                return Comparator.comparing(EventDTO::getCountry);

            default:
                return null;
        }
    }

}
