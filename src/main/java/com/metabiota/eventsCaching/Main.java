package com.metabiota.eventsCaching;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.metabiota.eventsCaching.util.TypedProperties;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;

import static com.metabiota.eventsCaching.Constants.*;

public class Main {

    private static final String DEFAULT_EVENTS_FILE = "/resources/prod_events.csv";

    private static final Logger log = Logger.getLogger(Main.class);

    @Parameter(names={"-c", "config"}, description = "configuration file", required = true)
    private String configFile;

    @Parameter(names={"-e", "-eventsFile"}, description = "The path to the events file in predefined CSV format")
    private String eventFile;

    public static void main(String[] args) throws Exception {
        Main main = new Main();
        new JCommander(main, args);
        main.run();
    }

    private void run() throws Exception {

        log.info("All pathogen count:" + PATHOGENS.size());
        log.info("All cc count: " + COUNTRY_CODES.size());
        List<EventDTO> events = loadEvents();
        log.info("All events count: " + events.size());

        TypedProperties conf = loadProperties(configFile);
        log.info("Conf: " + conf);

        Benchmark benchmark = new Benchmark(conf).setAllEvents(events);
        benchmark.run();
        benchmark.report();
    }

    private TypedProperties loadProperties(String s) throws IOException {
        File file = new File(s);
        log.info("Configuration file at " + file.getAbsolutePath());
        try (InputStreamReader reader = new FileReader(file)) {
            Properties conf = new Properties();
            conf.load(reader);
            return TypedProperties.from(conf);
        }
    }

    private List<EventDTO> loadEvents() throws IOException {
        List<EventDTO> events;
        InputStream eventStream = null;
        try {
            if (eventFile != null) {
                log.info("Event path: " + new File(eventFile).getAbsolutePath());
                eventStream = new BufferedInputStream(new FileInputStream(eventFile));
            } else {
                log.info("Using default events data");
                eventStream = Main.class.getResourceAsStream(DEFAULT_EVENTS_FILE);
            }
            CsvSchema schema = CsvSchema.emptySchema()
                    .withHeader() // use first row as header; otherwise defaults are fine
                    .withQuoteChar('\'');
            CsvMapper mapper = new CsvMapper();

            try (MappingIterator<Map<String, String>> it = mapper.readerFor(Map.class)
                    .with(schema)
                    .readValues(eventStream)) {
                EventCsvMapper eventMapper = new EventCsvMapper(ALL_PATHOGENS, ALL_COUNTRY_CODES);
                events = eventMapper.readEvents(it);
            }
            Collections.sort(events, Comparator.comparingInt(EventDTO::getCumlCases).reversed());
        }
        finally
        {
            if (eventStream != null) {
                try { eventStream.close(); } catch (Throwable t) {}
            }
        }
        return events;
    }
}
