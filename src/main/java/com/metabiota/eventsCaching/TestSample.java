/*
 *************************************************************************
 *  Copyright (c) Metabiota Incorporated - All Rights Reserved
 *------------------------------------------------------------------------
 *  This material is proprietary to Metabiota Incorporated. The
 *  intellectual and technical concepts contained herein are proprietary
 *  to Metabiota Incorporated. Reproduction or distribution of this
 *  material, in whole or in part, is strictly forbidden unless prior
 *  written permission is obtained from Metabiota Incorporated.
 *************************************************************************
 */

package com.metabiota.eventsCaching;

import com.metabiota.eventsCaching.util.FastContains;
import com.metabiota.eventsCaching.util.QueryUtil;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestSample {

    private int numPathogens;
    private int numCountries;
    private int limit;
    private boolean presorted;
    private String orderBy;

    private Query query;

    public TestSample setNumPathogens(int numPathogens) {
        this.numPathogens = numPathogens;
        return this;
    }

    public TestSample setNumCountries(int numCountries) {
        this.numCountries = numCountries;
        return this;
    }

    public TestSample setPresorted(boolean presorted) {
        this.presorted = presorted;
        return this;
    }

    public TestSample setOrderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public TestSample setLimit(int limit) {
        this.limit = limit;
        return this;
    }

    public void init() {
        query = randomQuery();

    }

    public List<EventDTO> doFilter(List<EventDTO> events) {
        Comparator<EventDTO> orderByComparator = QueryUtil.getOrderBy(orderBy);

        Stream<EventDTO> stream = events.stream()
                .filter(event -> query.accepts(event));
        if (!presorted && orderByComparator != null) {
            stream = stream.sorted(orderByComparator);
        }
        return stream.limit(limit)
                .collect(Collectors.toList());
    }

    private Query randomQuery() {
        Query query = new Query();
        query.setPathogens(getRandom(Constants.PATHOGENS, Constants.ALL_PATHOGENS, numPathogens));
        query.setCountryCodes(getRandom(Constants.COUNTRY_CODES, Constants.ALL_COUNTRY_CODES, numCountries));
        return query;
    }

    private BitSet getRandom(Set<String> collection, FastContains membershipIds, int num) {
        List<String> all = new ArrayList<>(collection);
        Collections.shuffle(all);
        Set<String> randElements = new HashSet<>(all.subList(0, num));
        return membershipIds.createMembership(randElements);
    }

    @Override
    public String toString() {
        return "TestSample{" +
                "numPathogens=" + numPathogens +
                ", numCountries=" + numCountries +
                ", limit=" + limit +
                ", query=" + query +
                '}';
    }
}
