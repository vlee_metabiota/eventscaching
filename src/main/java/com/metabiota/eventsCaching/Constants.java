/*
 *************************************************************************
 *  Copyright (c) Metabiota Incorporated - All Rights Reserved
 *------------------------------------------------------------------------
 *  This material is proprietary to Metabiota Incorporated. The
 *  intellectual and technical concepts contained herein are proprietary
 *  to Metabiota Incorporated. Reproduction or distribution of this
 *  material, in whole or in part, is strictly forbidden unless prior
 *  written permission is obtained from Metabiota Incorporated.
 *************************************************************************
 */

package com.metabiota.eventsCaching;

import com.google.common.collect.ImmutableSet;
import com.metabiota.eventsCaching.util.FastContains;

public class Constants {

    public static final String CONF_NUM_COUNTRIES = "numCountries";
    public static final String CONF_NUM_PATHOGENS = "numPathogens";
    public static final String CONF_ORDER_BY = "orderBy";
    public static final String CONF_RESULT_LIMIT = "limit";
    public static final String CONF_NUM_RUNS = "numRuns";
    public static final String CONF_PRESORTED = "presorted";
    public static final String CONF_NUM_WARMUP_RUNS = "warmupRuns";

    public static final ImmutableSet<String> COUNTRY_CODES = ImmutableSet.<String>builder()
            .add("AE", "AF", "AG", "AI", "AL", "AM", "AO", "AR", "AS", "AT", "AU", "AW", "AZ", "BA", "BB", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BL", "BM", "BN", "BO", "BQ", "BR", "BS", "BT", "BW", "BY", "BZ", "CA", "CD", "CF", "CG", "CH", "CI", "CK", "CL", "CM", "CN", "CO", "CR", "CU", "CV", "CW", "CY", "CZ", "DE", "DJ", "DK", "DM", "DO", "DZ", "EC", "EE", "EG", "ES", "ET", "EU", "FI", "FJ", "FM", "FR", "GA", "GB", "GD", "GE", "GF", "GG", "GH", "GI", "GL", "GM", "GN", "GP", "GQ", "GR", "GT", "GU", "GW", "GY", "HK", "HN", "HR", "HT", "HU", "ID", "IE", "IL", "IM", "IN", "IQ", "IR", "IS", "IT", "JE", "JM", "JO", "JP", "KE", "KG", "KH", "KI", "KM", "KN", "KR", "KW", "KY", "KZ", "LA", "LB", "LC", "LK", "LR", "LS", "LT", "LU", "LV", "LY", "MA", "MC", "MD", "ME", "MF", "MG", "MH", "MK", "ML", "MM", "MN", "MO", "MQ", "MR", "MS", "MT", "MU", "MV", "MW", "MX", "MY", "MZ", "NA", "NC", "NE", "NG", "NI", "NL", "NO", "NP", "NR", "NZ", "OM", "PA", "PE", "PF", "PG", "PH", "PK", "PL", "PR", "PS", "PT", "PW", "PY", "QA", "RE", "RO", "RS", "RU", "RW", "SA", "SB", "SC", "SD", "SE", "SG", "SI", "SK", "SL", "SN", "SO", "SR", "SS", "ST", "SV", "SX", "SY", "SZ", "TC", "TD", "TG", "TH", "TJ", "TL", "TM", "TN", "TO", "TR", "TT", "TW", "TZ", "UA", "UG", "US", "UY", "UZ", "VC", "VE", "VG", "VI", "VN", "VU", "WS", "XK", "YE", "YT", "ZA", "ZM", "ZW")
            .build();

    public static final FastContains ALL_COUNTRY_CODES = new FastContains(COUNTRY_CODES);

    public static final ImmutableSet<String> PATHOGENS = ImmutableSet.<String>builder()
            .add("Abnormal prion protein", "Alkhumra virus", "Bacillus anthracis", "Bordetella pertussis", "Borrelia recurrentis", "Borrelia sp.", "Bromide", "Buffalopox virus", "Bundibugyo ebolavirus", "Burkholderia pseudomallei", "Caraparu virus", "Chandipura virus", "Chikungunya virus", "Clostridium botulinum", "Coccidioides immitis", "Congenital Zika syndrome", "Coronavirus OC43", "Corynebacterium diphtheriae", "Coxsackievirus B3", "Coxsackievirus B5", "Crimean-Congo hemorrhagic fever virus", "Dengue virus", "Elizabethkingia anophelis", "Enterohemorrhagic E. coli", "Enterovirus A71", "Enterovirus D68", "Enterovirus D70", "Francisella tularensis", "Haemophilus influenzae", "Hantavirus", "Hendra virus", "Hepatitis A virus", "Hepatitis E virus", "Japanese encephalitis virus", "Junin virus", "Kyasanur Forest disease virus", "Lassa virus", "Legionella pneumophila", "Legionella sp.", "Leishmania sp.", "Leptospira sp.", "Listeria monocytogenes", "Lujo virus", "MERS Coronavirus", "Machupo virus", "Marburg virus", "Mayaro virus", "Measles virus", "Melamine", "Monkeypox virus", "Multipathogen", "Mumps virus", "Neisseria meningitidis", "Nipah virus", "Non-Shiga toxin-producing E. coli", "Norovirus", "Novel Influenza", "Oropouche virus", "Pandemic Influenza", "Plasmodium falciparum", "Plasmodium sp.", "Plasmodium vivax", "Poliovirus", "Rabies virus", "Reston ebolavirus", "Rickettsia prowazekii", "Rift Valley fever virus", "Rocio virus", "Ross River virus", "SARS Coronavirus", "Salmonella", "Seasonal Influenza", "Severe fever with thrombocytopenia syndrome virus", "Shiga toxin-producing E. coli", "Shigella dysenteriae", "Shigella sp.", "Staphylococcus aureus", "Streptococcus pneumoniae", "Sudan ebolavirus", "Tai Forest ebolavirus", "Uncertain etiology", "Vaccinia virus", "Venezuelan equine encephalitis virus", "Vibrio cholerae", "West Nile virus", "Yellow fever virus", "Yersinia pestis", "Zaire ebolavirus", "Zika virus")
            .build();

    public static final FastContains ALL_PATHOGENS = new FastContains(PATHOGENS);
}
