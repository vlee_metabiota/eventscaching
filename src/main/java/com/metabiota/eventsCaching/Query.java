/*
 *************************************************************************
 *  Copyright (c) Metabiota Incorporated - All Rights Reserved
 *------------------------------------------------------------------------
 *  This material is proprietary to Metabiota Incorporated. The
 *  intellectual and technical concepts contained herein are proprietary
 *  to Metabiota Incorporated. Reproduction or distribution of this
 *  material, in whole or in part, is strictly forbidden unless prior
 *  written permission is obtained from Metabiota Incorporated.
 *************************************************************************
 */

package com.metabiota.eventsCaching;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

public class Query {

    private Set<String> pathogens = new HashSet<>();

    private Set<String> countryCodes = new HashSet<>();

    private int casesLow = 0;
    private int casesHigh = Integer.MAX_VALUE;

    private int deathsLow = 0;
    private int deathsHigh = Integer.MAX_VALUE;

    private long startDate = Long.MIN_VALUE;
    private long endDate = Long.MAX_VALUE;

    private long globalStartDate = Long.MIN_VALUE;
    private long globalEndDate = Long.MAX_VALUE;

    private BitSet allPathogens;
    private BitSet allCountries;

//    public Query setPathogens(Set<String> pathogens) {
//        this.pathogens = pathogens;
//        return this;
//    }
//
//    public Query setCountryCodes(Set<String> countryCodes) {
//        this.countryCodes = countryCodes;
//        return this;
//    }

    public Query setPathogens(BitSet allPathogens) {
        this.allPathogens = allPathogens;
        return this;
    }

    public Query setCountryCodes(BitSet allCountries) {
        this.allCountries = allCountries;
        return this;
    }

    public Query setCasesLow(int casesLow) {
        this.casesLow = casesLow;
        return this;
    }

    public Query setCasesHigh(int casesHigh) {
        this.casesHigh = casesHigh;
        return this;
    }

    public Query setDeathsLow(int deathsLow) {
        this.deathsLow = deathsLow;
        return this;
    }

    public Query setDeathsHigh(int deathsHigh) {
        this.deathsHigh = deathsHigh;
        return this;
    }

    public Query setStartDate(long startDate) {
        this.startDate = startDate;
        return this;
    }

    public Query setEndDate(long endDate) {
        this.endDate = endDate;
        return this;
    }

    public Query setGlobalStartDate(long globalStartDate) {
        this.globalStartDate = globalStartDate;
        return this;
    }

    public Query setGlobalEndDate(long globalEndDate) {
        this.globalEndDate = globalEndDate;
        return this;
    }

    public boolean accepts(EventDTO e) {
        Integer cumlCases = e.getCumlCases();
        if (cumlCases != null) {
            boolean pass = cumlCases >= casesLow && cumlCases <= casesHigh;
            if (!pass) {
                return false;
            }
        }

        Integer cumlDeaths = e.getCumlDeaths();
        if (cumlDeaths != null) {
            boolean pass = cumlDeaths >= deathsLow && cumlDeaths <= deathsHigh;
            if (!pass) {
                return false;
            }
        }

        Long eStart = e.getStartDate();
        if (eStart != null && eStart >= endDate) {
            return false;
        }

        Long eEnd = e.getEndDate();
        if (eEnd != null && eEnd <= startDate) {
            return false;
        }

        Long gStart = e.getGlobalStartDate();
        if (gStart != null && eStart >= globalEndDate) {
            return false;
        }
        Long gEnd = e.getGlobalEndDate();
        if (gEnd != null && eEnd <= globalStartDate) {
            return false;
        }

        if (e.getCountryCodeId() != null && !allCountries.get(e.getCountryCodeId())) {
            return false;
        }

        if (e.getPathogenId() != null && !allPathogens.get(e.getPathogenId())) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Query{" +
                "pathogens=" + pathogens +
                ", countryCodes=" + countryCodes +
                ", casesLow=" + casesLow +
                ", casesHigh=" + casesHigh +
                ", deathsLow=" + deathsLow +
                ", deathsHigh=" + deathsHigh +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", globalStartDate=" + globalStartDate +
                ", globalEndDate=" + globalEndDate +
                '}';
    }
}
