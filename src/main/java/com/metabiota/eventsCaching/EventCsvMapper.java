/*
 *************************************************************************
 *  Copyright (c) Metabiota Incorporated - All Rights Reserved
 *------------------------------------------------------------------------
 *  This material is proprietary to Metabiota Incorporated. The
 *  intellectual and technical concepts contained herein are proprietary
 *  to Metabiota Incorporated. Reproduction or distribution of this
 *  material, in whole or in part, is strictly forbidden unless prior
 *  written permission is obtained from Metabiota Incorporated.
 *************************************************************************
 */

package com.metabiota.eventsCaching;

import com.metabiota.eventsCaching.util.FastContains;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class EventCsvMapper {

    private static final Logger log = Logger.getLogger(EventCsvMapper.class);

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    private FastContains allPathogens;
    private FastContains allCountries;

    public EventCsvMapper(FastContains allPathogens, FastContains allCountries) {
        this.allPathogens = allPathogens;
        this.allCountries = allCountries;
    }

    public List<EventDTO> readEvents(Iterator<Map<String, String>> it) {
        List<EventDTO> events = new ArrayList<>();
        while (it.hasNext()) {
            events.add(toEvent(it.next()));
        }
        return events;
    }

    public EventDTO toEvent(Map<String, String> map) {
        EventDTO dto = new EventDTO();
        int row = 0;
        for (Map.Entry<String, String> entry : map.entrySet()) {
            row++;
            String value = entry.getValue();
            String key = entry.getKey();
            try {
                switch (key) {
                    case "EVENT_ID":
                        dto.setEventId(getInteger(value));
                        break;
                    case "CC":
                        dto.setCountryCode(getString(value));
                        break;
                    case "PATHOGEN":
                        dto.setPathogen(getString(value));
                        break;
                    case "EVENT":
                        dto.setEventName(getString(value));
                        break;
                    case "SRC":
                        dto.setSrc(getString(value));
                        break;
                    case "COUNTRY":
                        dto.setCountry(getString(value));
                        break;
                    case "DISEASE":
                        dto.setDisease(getString(value));
                        break;
                    case "DATE_START":
                        dto.setStartDate(getEpoch(value));
                        break;
                    case "DATE_END":
                        dto.setEndDate(getEpoch(value));
                        break;
                    case "GLOBAL_DATE_START":
                        dto.setGlobalStartDate(getEpoch(value));
                        break;
                    case "GLOBAL_DATE_END":
                        dto.setGlobalEndDate(getEpoch(value));
                        break;
                    case "COUNTRY_ORIG":
                        dto.setCountryOrig(getString(value));
                        break;
                    case "CFR":
                        dto.setCfr(getDouble(value));
                        break;
                    case "CUML_CASES":
                        dto.setCumlCases(getInteger(value));
                        break;
                    case "CUML_DEATHS":
                        dto.setCumlDeaths(getInteger(value));
                        break;
                    case "R0_EG":
                        dto.setR0EG(getDouble(value));
                        break;
                    case "R0_ML":
                        dto.setR0ML(getDouble(value));
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                log.error(String.format("Error at row %d, key: %s, value: %s", row, key, value));
            }
            if (dto.getPathogen() != null) {
                dto.setPathogenId(allPathogens.getMembershipId(dto.getPathogen()));
            }
            if (dto.getCountry() != null) {
                dto.setCountryCodeId(allCountries.getMembershipId(dto.getCountryCode()));
            }
        }
        return dto;
    }

    private static Double getDouble(String s) {
        return getString(s) == null ? null : Double.parseDouble(s);
    }

    private static Integer getInteger(String s) {
        return getString(s) == null ? null : Integer.parseInt(s);
    }

    private static String getString(String s) {
        return getString(s, null);
    }

    private static Long getEpoch(String s) {
        return (getString(s) == null) ? null :
            LocalDateTime.parse(s, formatter).atZone(ZoneId.of("UTC")).toInstant().toEpochMilli();
    }

    private static String getString(String s, String defValue) {
        return (s == null || s.equals("") || s.equals("null")) ? defValue : s;
    }
}
