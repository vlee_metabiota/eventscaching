/*
 *************************************************************************
 *  Copyright (c) Metabiota Incorporated - All Rights Reserved
 *------------------------------------------------------------------------
 *  This material is proprietary to Metabiota Incorporated. The
 *  intellectual and technical concepts contained herein are proprietary
 *  to Metabiota Incorporated. Reproduction or distribution of this
 *  material, in whole or in part, is strictly forbidden unless prior
 *  written permission is obtained from Metabiota Incorporated.
 *************************************************************************
 */

package com.metabiota.eventsCaching;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.metabiota.eventsCaching.util.QueryUtil;
import com.metabiota.eventsCaching.util.TypedProperties;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.metabiota.eventsCaching.Constants.*;

public class Benchmark {

    private static Logger log = Logger.getLogger(Benchmark.class);

    private static final MetricRegistry metrics = new MetricRegistry();
    private Histogram filterNanos = metrics.histogram("filterNanos");

    private ConsoleReporter reporter;

    private List<EventDTO> allEvents;
    private int numRuns;
    private int numCountries;
    private int numPathogens;
    private String orderBy;
    private int limit;
    private boolean presorted;
    private int warmupRuns;

    public Benchmark(TypedProperties conf) {
        setConfiguration(conf);
        reporter = ConsoleReporter.forRegistry(metrics)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.NANOSECONDS)
                .build();
    }

    public Benchmark setAllEvents(List<EventDTO> allEvents) {
        this.allEvents = new ArrayList<>(allEvents);
        if (presorted) {
            Comparator<EventDTO> comp = QueryUtil.getOrderBy(orderBy);
            if (comp != null) {
                Collections.sort(this.allEvents, comp);
            }
        }
        return this;
    }

    private void setConfiguration(TypedProperties conf) {
        numRuns = conf.getInteger(CONF_NUM_RUNS, 10000);
        numCountries = conf.getInteger(CONF_NUM_COUNTRIES, 3);
        numPathogens = conf.getInteger(CONF_NUM_PATHOGENS, 1);
        orderBy = conf.getString(CONF_ORDER_BY, null);
        limit = conf.getInteger(CONF_RESULT_LIMIT, 25);
        presorted = conf.getBoolean(CONF_PRESORTED, false);
        warmupRuns = conf.getInteger(CONF_NUM_WARMUP_RUNS, 1000);
    }

    public void run() {
        // warm up
        for (int i = 0; i < warmupRuns; i++) {
            TestSample sample = new TestSample()
                    .setNumCountries(numCountries)
                    .setNumPathogens(numPathogens)
                    .setLimit(limit);
            sample.init();
            sample.doFilter(allEvents);
        }

        // real run
        for (int i = 0; i < numRuns; i++) {
            TestSample sample = new TestSample()
                    .setNumCountries(numCountries)
                    .setNumPathogens(numPathogens)
                    .setOrderBy(orderBy)
                    .setPresorted(presorted)
                    .setLimit(limit);
            sample.init();

            long start = System.nanoTime();
            List<EventDTO> results = sample.doFilter(allEvents);
            long duration = System.nanoTime() - start;
            filterNanos.update(duration);
            if (log.isDebugEnabled()) {
                log.debug(String.format("%d ns (%d)", duration, results.size()));
            }
        }
    }

    public void report() {
        reporter.report();
    }

}
